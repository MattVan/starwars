import React, {Component} from 'react';
import axios from "axios";

class Characters extends Component {

    constructor(props) {
        super(props);

        this.state = {
            characters: []
        }
    }

    componentDidMount() {
        this.mountCharacters('https://swapi.dev/api/people');
    }

    //correctly capturing 82 characters
    mountCharacters = (url) => {
        axios.get(`${url}`)
            .then(response => {
                this.setState({
                    characters:[...this.state.characters,...response.data.results],
                    next:response.data.next
                });

                if(response.data.next){
                    this.mountCharacters(response.data.next);
                }
            })
            .catch(err => {
                //handle error here
                console.log(err);
            })
    }

    render() {

        if (this.state.characters.length < 1) {
            console.log('waiting for characters')

            return <div>Waiting...</div>;
        }

        // console.log(this.state.characters);

        return (
            <div>
                Characters
            </div>
        );
    }
}

export default Characters;