import React, {Component} from 'react';
import axios from "axios";


class Vehicles extends Component {

    constructor(props) {
        super(props);

        this.state = {
            vehicles: []
        }
    }

    componentDidMount() {
        this.mountVehicles('https://swapi.dev/api/planets')
    }

    mountVehicles = (url) => {
        axios.get(`${url}`)
            .then(response => {
                this.setState({
                    vehicles: [...this.state.vehicles, ...response.data.results],
                    next: response.data.next
                });

                if (response.data.next) {
                    this.mountVehicles(response.data.next);
                }
            })
            .catch(err => {
                //handle error here
                console.log(err);
            })
    }

    render() {
        if (this.state.vehicles.length < 1) {
            console.log('waiting for vehicles')

            return <div>Waiting...</div>;
        }

        console.log(this.state.vehicles);

        return (
            <div>
                vehicles
            </div>
        );
    }
}

export default Vehicles;