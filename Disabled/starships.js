import React, {Component} from 'react';
import axios from 'axios';

class Starships extends Component {

    constructor(props) {
        super(props);

        this.state = {
            starships: []
        }
    }

    componentDidMount() {
        this.mountStarships('https://swapi.dev/api/starships');
    }

    mountStarships = (url) => {
        axios.get(`${url}`)
            .then(response => {
                this.setState({
                    starships: [...this.state.starships, ...response.data.results],
                    next: response.data.next
                });

                if (response.data.next) {
                    this.mountStarships(response.data.next);
                }
            })
            .catch(err => {
                //handle error here
                console.log(err);
            })
    }

    render() {

        if (this.state.starships.length < 1) {
            console.log('waiting for starships')

            return <div>Waiting...</div>;
        }

        console.log(this.state.starships);

        return (
            <div>
                Starships
            </div>
        );
    }
}

export default Starships;