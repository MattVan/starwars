import React, {Component} from 'react';
import axios from "axios";

class Planets extends Component {

    constructor(props) {
        super(props);

        this.state = {
            planets: []
        }
    }

    componentDidMount() {
        this.mountPlanets('https://swapi.dev/api/planets')
    }

    mountPlanets = (url) => {
        axios.get(`${url}`)
            .then(response => {
                this.setState({
                    planets: [...this.state.planets, ...response.data.results],
                    next: response.data.next
                });

                if (response.data.next) {
                    this.mountPlanets(response.data.next);
                }
            })
            .catch(err => {
                //handle error here
                console.log(err);
            })
    }

    render() {
        if (this.state.planets.length < 1) {
            console.log('waiting for planets')

            return <div>Waiting...</div>;
        }

        console.log(this.state.planets);

        return (
            <div>
                Planets
            </div>
        );
    }
}

export default Planets;