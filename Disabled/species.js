import React, {Component} from 'react';
import axios from "axios";

class Species extends Component {

    constructor(props) {
        super(props);

        this.state = {
            species: []
        }
    }

    componentDidMount() {
        this.mountSpecies('https://swapi.dev/api/species')
    }

    mountSpecies = (url) => {
        axios.get(`${url}`)
            .then(response => {
                this.setState({
                    species: [...this.state.species, ...response.data.results],
                    next: response.data.next
                });

                if (response.data.next) {
                    this.mountSpecies(response.data.next);
                }
            })
            .catch(err => {
                //handle error here
                console.log(err);
            })
    }

    render() {
        if (this.state.species.length < 1) {
            console.log('waiting for species')

            return <div>Waiting...</div>;
        }

        console.log(this.state.species);

        return (
            <div>
                Species
            </div>
        );
    }
}

export default Species;