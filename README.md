#Star Wars API Project

Ideas for improvements that could be made to the application: 
- Add a sign in feature with profile where you could save favorite items (movies, planets, characters, etc)
- Profile could contain an avatar ex. I could pick Chewbacca for mine :)
- There is a Wookie speak option on the api, so perhaps an i18n translation for translating the entire page to Wookie speak
- As stated above, I feel like this whole application could run off of a central template (similar to src/Templates/itemDetails) to increase speed and ease extensibility
- Implementation of testing to frontend react & axios mock calls using JEST
- Pictures of each item inside of the oneItem pages.  These could be through server calls which might make the application very resource intensive, or through common images stored in public/images folders
- Styling of course, I spent my time on the application not the CSS
- Comment section if signed in to leave comments about each item specific page
- Add an error boundary as opposed to locally rendering errors in a template component.  We could wrap each component in the error boundary which would take care of all thrown errors inside the application
- Clean up route names - currently "people" from the api is labelled as 'characters' because I felt that was a better identifier.  This created some redundant code (switch statements etc) to handle data correctly 
- Add redux store to application since state management can become unmanageably large with comments, profiles, authentication, etc.

#Roadmap: Completed

Create class based components which:
    Movies: Handles getting all movies
    oneMovie: Displays one movie using functional components below
    oneCharacter, oneSpecies, oneStarship, oneVehicle, onePlanet: Displays information based on label using functional components   
    -done

Create functional components which:
    Display Movie, vehicle, character, starship, planet, or species Details  
    -done switched to class based to own their own states
    
Create search function which ties into the routes with /:id  
    -done

Create error handling component which renders error messages  
    -done
