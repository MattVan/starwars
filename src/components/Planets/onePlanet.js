import React, {Component} from 'react';
import axios from "axios";
import ItemDetails from "../Templates/itemDetails";
import {Link} from "react-router-dom";
import ErrorHandler from "../Errors/errorHandler";

class OnePlanet extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount() {
        this.mountPlanet();
    }

    mountPlanet = () => {
        axios.get(`https://swapi.dev/api/planets/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    planet: response.data
                })
            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors: err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors: err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors: err.message
                    })

                }
            })
    }

    displayPlanet = () => {
        let planet = this.state.planet;

        return (
            <div>
                <h1>
                    {planet.name}
                </h1>
                <h2>Details:</h2>
                <div>
                    Climate: {planet.climate}
                    <br/>
                    Diameter (km): {planet.diameter}
                    <br/>
                    Gravity (G's): {planet.gravity}
                    <br/>
                    Orbital Period (days): {planet.orbital_period}
                    <br/>
                    Population (sentient beings): {planet.population}
                    <br/>
                    Rotation Period (hrs/rotation): {planet.rotation_period}
                    <br/>
                    Surface Water (%): {planet.surface_water}
                    <br/>
                    Terrain: {planet.terrain}
                </div>
                <div>
                    <h2>Appears In:</h2>
                    <ItemDetails
                        items={planet.films}
                        msg={'Not in any films yet'}
                        options={{
                            type: 'movie'
                        }}
                    />
                </div>
                <div>
                    <h2>Residents:</h2>
                    <ItemDetails
                        items={planet.residents}
                        msg={'No Known Residents'}
                        options={{
                            type: 'character'
                        }}
                    />
                </div>
                <br/>
                <div>
                    <Link to={'/'}>Take Me Back Home</Link>
                </div>
            </div>
        )

    }

    render() {
        if (this.state.errors) {
            return (
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }
        if (!this.state.planet) {
            return (
                <div>
                    Loading Planet Information...
                </div>
            )
        }


        // console.log(this.state.planet);
        return (
            <div>
                {this.displayPlanet()}
            </div>
        );
    }
}

export default OnePlanet;