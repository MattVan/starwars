import React, {Component} from 'react';
import axios from "axios";
import ItemDetails from "../Templates/itemDetails";
import {Link} from "react-router-dom";
import ErrorHandler from "../Errors/errorHandler";

class OneCharacter extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount() {
        this.mountCharacter();
    }

    mountCharacter = () => {
        axios.get(`https://swapi.dev/api/people/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    character: response.data
                })
            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors:err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors:err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors:err.message
                    })

                }
            })
    }

    displayCharacter = () => {
        let char = this.state.character;

        return (
            <div>
                <h1>
                    {char.name}
                </h1>
                <h2>Details:</h2>
                <div>
                    Born in: {char.birth_year}
                    <br/>
                    Gender: {char.gender}
                    <br/>
                    Height (cm): {char.height}
                    <br/>
                    Weight (kg): {char.mass}
                    <br/>
                    Skin Color: {char.skin_color}
                    <br/>
                    Eye Color: {char.eye_color}
                    <br/>
                    Hair Color: {char.hair_color}
                </div>
                <div>
                    <h2>Homeworld:</h2>
                    <div>
                        <ItemDetails
                            items={[char.homeworld]}
                            msg={'No Homeworld, a drifter one would say'}
                            options={{
                                type: 'planet'
                            }}
                        />
                    </div>
                </div>
                <div>
                    <h2>Appears In:</h2>
                    <ItemDetails
                        items={char.films}
                        msg={'Not in any films yet, check back soon'}
                        options={{
                            type: 'movie'
                        }}
                    />
                </div>

                <div>
                    <h2>Species:</h2>
                    <ItemDetails
                        items={char.species}
                        msg={'Not specified, appears to be human'}
                        options={{
                            type: 'species'
                        }}
                    />
                </div>

                <div>
                    <h2>Starships:</h2>
                    <ItemDetails
                        items={char.starships}
                        msg={'No Starships'}
                        options={{
                            type: 'starship'
                        }}
                    />
                </div>
                <div>
                    <h2>Vehicles:</h2>
                    <ItemDetails
                        items={char.vehicles}
                        msg={'No Vehicles'}
                        options={{
                            type: 'vehicle'
                        }}
                    />
                </div>
                <br/>
                <div>
                    <Link to={'/'}>Take Me Back Home</Link>
                </div>

            </div>
        )

    }

    render() {
        if (this.state.errors) {
            return (
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }
        if (!this.state.character) {
            return (
                <div>
                    Loading Character Information...
                </div>
            )
        }


        // console.log(this.state);
        return (
            <div>
                {this.displayCharacter()}
            </div>
        );
    }
}

export default OneCharacter;