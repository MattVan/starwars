import React, {Component} from 'react';
import axios from "axios";
import {Link} from "react-router-dom";

class ItemDetails extends Component {


    constructor(props) {
        super(props);

        this.state = {
            items: []
        }
    }


    componentDidMount() {
        this.mountItems();
    }

    mountItems = () => {

        return this.props.items.forEach(item => {
            return axios.get(item)
                .then(response => {
                    // console.log(response.data.url.trim().split('/'));

                    this.setState({
                        items: [...this.state.items, response.data]
                    })
                })
                .catch(err => {
                    this.setState({
                        errors:err
                    })
                });
        });
    }

    displayItems = () => {
        return this.state.items.map(item => {
            let splitURL = item.url.trim().split('/');
            //trial and error to find this with data

            let num = splitURL[splitURL.length - 2];

            let name = (item.name || item.title);

            return (
                <li key={name}>
                    <Link to={`/one${this.props.options.type}/${num}/`}>
                        {name}
                    </Link>
                </li>
            );
        })

    }

    render() {
        if (this.props.items.length > this.state.items.length) {
            return (
                <div>
                    Loading information
                </div>
            );
        }
        if (this.state.items.length === 0) {
            return (
                <div>
                    {this.props.msg}
                </div>

            )
        }

        return (
            <div>
                <ul>
                    {this.displayItems()}
                </ul>
            </div>
        )
    }
}

export default ItemDetails;