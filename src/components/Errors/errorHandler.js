import React, {Component} from 'react';

class ErrorHandler extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    renderErrors = () => {
        return (
            <div>
                <h1>Error:</h1>
                {this.props.errors}
            </div>
        )
    }

    render() {
        // console.log(this.props.errors);
        return (
            <div>
                {this.renderErrors()}
            </div>

        );
    }
}

export default ErrorHandler;