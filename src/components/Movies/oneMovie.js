import React, {Component} from 'react';
import axios from "axios";

import ItemDetails from "../Templates/itemDetails";
import {Link} from "react-router-dom";
import ErrorHandler from "../Errors/errorHandler";

class OneMovie extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount() {
        this.mountMovie();
    }

    mountMovie = () => {
        axios.get(`https://swapi.dev/api/films/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    movie: response.data
                })
            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors:err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors:err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors:err.message
                    })

                }
            })
    }

    displayOneMovie = () => {
        const movie = this.state.movie;

        const template = (
            <div className={'oneMovie'} key={movie.episode_id}>
                <h1>
                    Star Wars: Episode {movie.episode_id}, {movie.title}
                </h1>
                <h2>
                    Directed By: {movie.director}
                    <br/>
                    Produced By: {movie.producer}
                    <br/>
                    Released: {movie.release_date}
                </h2>

                <div>
                    <h1>Opening Crawl:</h1>
                    {movie.opening_crawl}
                </div>

                <div>
                    <h1>Characters:</h1>
                    <ItemDetails
                        items={movie.characters}
                        options={{
                            type: 'character'
                        }}
                    />
                </div>
                <div>
                    <h1>Planets:</h1>
                    <ItemDetails
                        items={movie.planets}
                        options={{
                            type: 'planet'
                        }}
                    />
                </div>
                <div>
                    <h1>Species</h1>
                    <ItemDetails
                        items={movie.species}
                        options={{
                            type: 'species'
                        }}
                    />
                </div>
                <div>
                    <h1>Starships</h1>
                    <ItemDetails
                        items={movie.starships}
                        options={{
                            type: 'starship'
                        }}
                    />
                </div>
                <div>
                    <h1>Vehicles</h1>
                    <ItemDetails
                        items={movie.vehicles}
                        options={{
                            type: 'vehicle'
                        }}
                    />
                </div>
                <br/>
                <div>
                    <Link to={'/'}>Take Me Back Home</Link>
                </div>
            </div>
        );

        return template;

    }

    render() {
        if (this.state.errors) {
            return (
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }
        if (!this.state.movie) {
            return (
                <div>
                    Waiting for movie data...
                </div>
            );
        }

        // console.log(this.state);

        return (
            <div>
                {this.displayOneMovie()}
            </div>
        )

    }
}

export default OneMovie;