import React, {Component} from 'react';
import axios from "axios";
import {Link} from "react-router-dom";

import {switchId} from '../../utils';
import SearchBar from "../Search/searchBar";
import ErrorHandler from "../Errors/errorHandler";

class Movies extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movies: []
        }
    }

    componentDidMount() {
        this.mountMovies();
    }

    mountMovies = () => {
        axios.get('https://swapi.dev/api/films')
            .then(response => {
                this.setState({
                    movies: [...this.state.movies, ...response.data.results]
                })

            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors: err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors: err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors: err.message
                    })

                }
            })
    }


    displayMovies = () => {

        const template = this.state.movies.map((item, i) => {
            //episode ID doesn't line up with the movie, so we have to alter the number here
            let id = switchId(item.episode_id);

            return (
                <div className={'movie-container'} key={i}>
                    <h1>
                        Star Wars: Episode {item.episode_id}, {item.title}
                    </h1>
                    <h2>
                        Directed By: {item.director}
                        <br/>
                        Produced By: {item.producer}
                        <br/>
                        Released: {item.release_date}
                    </h2>
                    <div>
                        <Link to={`/onemovie/${id}`}>
                            See More About This Movie
                        </Link>
                    </div>
                </div>
            );
        });

        return template;
    }

    render() {

        if (this.state.errors) {
            return(
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }

        if (this.state.movies.length < 1) {
            // console.log('waiting for movies')

            return <div>Waiting for movie information...</div>;
        }


        return (
            <div>
                <SearchBar/>
                {this.displayMovies()}
            </div>
        );
    }
}

export default Movies;