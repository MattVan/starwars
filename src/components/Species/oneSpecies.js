import React, {Component} from 'react';
import axios from "axios";
import ItemDetails from "../Templates/itemDetails";
import {Link} from "react-router-dom";
import ErrorHandler from "../Errors/errorHandler";

class OneSpecies extends Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.mountSpecies();
    }

    mountSpecies = () => {
        axios.get(`https://swapi.dev/api/species/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    species: response.data
                })
            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors:err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors:err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors:err.message
                    })

                }
            })
    }

    displaySpecies = () => {
        let species = this.state.species;

        return (
            <div>
                <h1>
                    {species.name}
                </h1>
                <h2>Details:</h2>
                <div>
                    Classification: {species.classification}
                    <br/>
                    Designation: {species.designation}
                    <br/>
                    Average Lifespan (years): {species.average_lifespan}
                    <br/>
                    Average Height (cm): {species.average_height}
                    <br/>
                    Eye Colors: {species.eye_colors}
                    <br/>
                    Hair Colors: {species.hair_colors}
                    <br/>
                    Skin Colors: {species.skin_colors}
                    <br/>
                    Language: {species.language}
                    <br/>
                </div>

                <div>
                    <h2>Homeworld:</h2>
                    <ItemDetails
                        items={[species.homeworld]}
                        msg={'No homeworld, must be a drifter'}
                        options={{
                            type: 'planet'
                        }}
                    />
                </div>
                <div>
                    <h2>Appears In:</h2>
                    <ItemDetails
                        items={species.films}
                        msg={'Not in any films yet'}
                        options={{
                            type: 'movie'
                        }}
                    />
                </div>
                <div>
                    <h2>{species.name} Characters:</h2>
                    <ItemDetails
                        items={species.people}
                        msg={'No known characters of this species'}
                        options={{
                            type: 'character'
                        }}
                    />
                </div>
                <br/>
                <div>
                    <Link to={'/'}>Take Me Back Home</Link>
                </div>
            </div>
        )

    }

    render() {

        if (this.state.errors) {
            return (
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }
        if (!this.state.species) {
            return (
                <div>
                    Loading Species Information...
                </div>
            )
        }

        // console.log(this.state.species);
        return (
            <div>
                {this.displaySpecies()}
            </div>
        );
    }
}

export default OneSpecies;