import React, {Component} from 'react';
import axios from "axios";
import ItemDetails from "../Templates/itemDetails";
import {Link} from "react-router-dom";
import ErrorHandler from "../Errors/errorHandler";

class OneStarship extends Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.mountStarship();
    }

    mountStarship = () => {
        axios.get(`https://swapi.dev/api/starships/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    starship: response.data
                })
            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors:err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors:err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors:err.message
                    })

                }
            })
    }

    displayStarship = () => {
        let ship = this.state.starship;
        if (ship.max_atmosphering_speed === 'N/A') {
            ship.max_atmosphering_speed = "Not Capable of Atmospheric Flight"
        }

        return (
            <div>
                <h1>
                    {ship.name}
                </h1>
                <h2>Details:</h2>
                <div>
                    Model: {ship.model}
                    <br/>
                    Starship Class: {ship.starship_class}
                    <br/>
                    Manufacturer: {ship.manufacturer}
                    <br/>
                    Cost (in credits): {ship.cost_in_credits}
                    <br/>
                    Crew Required (# of personnel): {ship.crew}
                    <br/>
                    Passengers (non-essential travellers): Able to transport {ship.passengers}
                    <br/>
                    Max Atmospheric Speed: {ship.max_atmosphering_speed}
                    <br/>
                    Hyperdrive Rating (class): {ship.hyperdrive_rating}
                    <br/>
                    Max Megalight Speed (megalights per hour): {ship.MGLT}
                    <br/>
                    Cargo Capacity (kg): {ship.cargo_capacity}
                    <br/>
                    Consumables without resupply: {ship.consumables}
                    <br/>
                </div>
                <div>
                    <h2>Appears In:</h2>
                    <ItemDetails
                        items={ship.films}
                        msg={'Not in any films yet'}
                        options={{
                            type: 'movie'
                        }}
                    />
                </div>
                <div>
                    <h2>Pilots:</h2>
                    <ItemDetails
                        items={ship.pilots}
                        msg={'No single pilot identified'}
                        options={{
                            type: 'character'
                        }}
                    />
                </div>
                <br/>
                <div>
                    <Link to={'/'}>Take Me Back Home</Link>
                </div>
            </div>
        )

    }

    render() {
        if (this.state.errors) {
            return(
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }

        if (!this.state.starship) {
            return (
                <div>
                    Loading Starship Information...
                </div>
            )
        }


        // console.log(this.state.starship);
        return (
            <div>
                {this.displayStarship()}
            </div>
        );
    }
}

export default OneStarship;