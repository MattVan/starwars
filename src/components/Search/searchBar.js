import React, {Component} from 'react';
import axios from "axios";
import ItemDetails from "../Templates/itemDetails";
import ErrorHandler from "../Errors/errorHandler";

class SearchBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedOption: 'films',
            subType: 'title',
            inputText: "",
            searchResult: [],
            linkURL: 'movie',
            done: false
        }
    }

    onValChange = (event) => {
        let _subType = 'name';

        if (event.target.value === 'films') {
            _subType = 'title'
        }
        let _linkURL = '';

        switch (event.target.value) {
            case('films'):
                _linkURL = 'movie'
                break;
            case('people'):
                _linkURL = 'character'
                break;
            case('vehicles'):
                _linkURL = 'vehicle'
                break;
            case('planets'):
                _linkURL = 'planet'
                break;
            case('starships'):
                _linkURL = 'starship'
                break;
            case('species'):
                _linkURL = 'species'
                break;

            default:
                _linkURL = null
        }

        this.setState({
            selectedOption: event.target.value,
            subType: _subType,
            inputText: '',
            linkURL: _linkURL
        });
    }

    formSubmit = (event) => {
        event.preventDefault();

        //reset results
        this.setState({
            searchResult: [],
            done: false
        })
        //get new query string
        let searchString = `http://swapi.dev/api/${this.state.selectedOption}/?search=${this.state.inputText}`;

        this.searchSubmit(searchString);

    }

    searchSubmit = (url) => {
        axios.get(url)
            .then(response => {
                //getting item urls for rendering list
                let results = response.data.results.map(item => {
                    return item.url;
                })
                //updating list with everything prior
                this.setState({
                    searchResult: [...this.state.searchResult, ...results]
                })

                //if theres more data, recursively call this function
                if (response.data.next) {
                    this.searchSubmit(response.data.next);
                } else {
                    //render items
                    this.setState({
                        done: true
                    })
                }
            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors: err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors: err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors: err.message
                    })

                }
            })

    }

//Kinda useless at this point, since we don't search for subtype but it lets the user know
// what they can search by at least...
    changeSubType = (event) => {
        this.setState({
            subType: event.target.value
        })
    }

    /*
    Search vals accepted:
    Characters: Name
    Films: title
    Species: Name
    Planets: name
    starships: name, model
    Vehicles: Name, model
     */

    renderOptions = () => {
        let template = null;

        switch (this.state.selectedOption) {
            case('films'):
                template = (
                    <select name="subType" onChange={this.changeSubType}>
                        <option value="title">Title</option>
                    </select>
                )
                break;
            case('starships' || 'vehicles'):
                template = (
                    <select name="subType" onChange={this.changeSubType}>
                        <option value="name">Name</option>
                        <option value="model">Model</option>
                    </select>

                )
                break;
            default:
                template = (
                    <select name="subType" onChange={this.changeSubType}>
                        <option value="name">Name</option>
                    </select>

                )
        }

        return template;
    }

    updateText = (event) => {
        // console.log(event.target.value);

        this.setState({
            inputText: event.target.value
        })
    }

    render() {
        if (this.state.errors) {
            return (
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }

        return (
            <div>
                <form onSubmit={this.formSubmit}>
                    Category:
                    <br/>

                    <select name="type" onChange={this.onValChange}>
                        <option value="films">Movies</option>
                        <option value="people">Characters</option>
                        <option value="vehicles">Vehicles</option>
                        <option value="planets">Planets</option>
                        <option value="starships">Starships</option>
                        <option value="species">Species</option>
                    </select>

                    <div>
                        <label>
                            Search By: <br/>
                            {this.renderOptions()}
                        </label>
                    </div>
                    <br/>
                    <label>
                        Search Value:
                        <input type="text" name={'searchVal'} onChange={this.updateText}
                               value={this.state.inputText}/>
                    </label>
                    <br/>
                    <button type={'submit'}>Submit</button>
                </form>

                <div>
                    {this.state.done ?
                        (
                            <div>
                                <h1>Search Results:</h1>
                                <ItemDetails
                                    items={this.state.searchResult}
                                    msg={this.state.errors || "Not Found"}
                                    options={{
                                        type: this.state.linkURL
                                    }}
                                />
                            </div>
                        ) : null
                    }

                </div>
            </div>
        );
    }
}

export default SearchBar;