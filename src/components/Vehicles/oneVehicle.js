import React, {Component} from 'react';
import axios from "axios";
import ItemDetails from "../Templates/itemDetails";
import {Link} from "react-router-dom";
import ErrorHandler from "../Errors/errorHandler";

class OneVehicle extends Component {

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.mountVehicle();
    }

    mountVehicle = () => {
        axios.get(`https://swapi.dev/api/vehicles/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    vehicle: response.data
                })
            })
            .catch(err => {
                if (err.response) {
                    //request responds with bad error code
                    this.setState({
                        errors:err.response
                    })
                } else if (err.request) {
                    //request made, no response
                    this.setState({
                        errors:err.request
                    })
                } else {
                    //error in setup
                    this.setState({
                        errors:err.message
                    })

                }
            })
    }

    displayVehicle = () => {
        let vehicle = this.state.vehicle;

        return (
            <div>
                <h1>
                    {vehicle.name}
                </h1>

                <h2>Details:</h2>

                <div>
                    Vehicle Class: {vehicle.vehicle_class}
                    <br/>
                    Manufactured By: {vehicle.manufacturer}
                    <br/>
                    Model: {vehicle.model}
                    <br/>
                    Length (meters): {vehicle.length}
                    <br/>
                    Crew Required to Operate (# personnel): {vehicle.crew}
                    <br/>
                    Max Speed In Atmosphere: {vehicle.max_atmosphering_speed}
                    <br/>
                    Cargo Capacity: {vehicle.cargo_capacity}
                    <br/>
                    Consumable Storage Without Resupply: {vehicle.consumables}
                    <br/>
                </div>
                <h2>
                    Appears In:
                </h2>
                <div>
                    <ItemDetails
                        items={vehicle.films}
                        msg={'No appearances in films yet'}
                        options={{
                            type: 'movie'
                        }}
                    />
                </div>
                <h2>
                    Piloted By:
                </h2>
                <div>
                    <ItemDetails
                        items={vehicle.pilots}
                        msg={'No Single Pilot Identified'}
                        options={{
                            type: 'character'
                        }}

                    />
                </div>
                <br/>
                <div>
                    <Link to={'/'}>Take Me Back Home</Link>
                </div>
            </div>
        )

    }

    render() {
        if (this.state.errors) {
            return (
                <div>
                    <ErrorHandler errors={this.state.errors}/>
                </div>
            )
        }
        if (!this.state.vehicle) {
            return (
                <div>
                    Loading Vehicle Information...
                </div>
            )
        }
        // console.log(this.state.vehicle);
        return (
            <div>
                {this.displayVehicle()}
            </div>
        );
    }
}

export default OneVehicle;