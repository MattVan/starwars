import React from 'react';
import {Switch, Route} from 'react-router-dom';

//components
import Movies from "./components/Movies/movies";
import OneMovie from "./components/Movies/oneMovie";
import OneCharacter from "./components/Characters/oneCharacter";
import OnePlanet from "./components/Planets/onePlanet";
import OneStarship from "./components/Starships/oneStarship";
import OneVehicle from "./components/Vehicles/oneVehicle";
import OneSpecies from "./components/Species/oneSpecies";

const Routes = () => {

    return (
        <div>
            <Switch>
                <Route path={'/'} exact component={Movies}/>
                <Route path={'/onemovie/:id'} exact component={OneMovie}/>

                <Route path={'/onecharacter/:id'} exact component={OneCharacter}/>

                <Route path={'/oneplanet/:id'} exact component={OnePlanet}/>

                <Route path={'/onestarship/:id'} exact component={OneStarship}/>

                <Route path={'/onevehicle/:id'} exact component={OneVehicle}/>

                <Route path={'/onespecies/:id'} exact component={OneSpecies}/>
            </Switch>
        </div>
    )

}

export default Routes;