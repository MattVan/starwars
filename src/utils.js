const switchId = (ep_id) => {
    switch (ep_id){
        case(1):
            return 4;
        case(2):
            return 5;
        case(3):
            return 6;
        case(4):
            return 1;
        case(5):
            return 2;
        case(6):
            return 3;
        default:
            return ep_id;
    }
}

module.exports = {
    switchId
}